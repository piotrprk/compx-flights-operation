import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Objects;

// class to represent table variables
// or table header
class TableVar {
	// variable name
	String name;
	// make hash from var name
	int hash;
	
	public String toString() {
		return name;
	}
	// check if contains column name
	// return column index
	public boolean contains(String name) {
		if (this.name.equals(name))
			return true;
		else
			return false;
	}
}

class TableSubject{
	// single row values
	HashMap<Integer, String> values;
	public TableSubject(HashMap<Integer, String> map){
		values = map;
	}
	
	public String toString() {
		String out= new String();
		for (Integer i:values.keySet()) {
			out += values.get(i) + "\t";
		}
		out += "\n";
		return out;
	}
	public String toString(int key) {
		return values.get(key) + "\n";
	}
	// print values with variable name/hash
	public String toString(ArrayList<TableVar> vars) {
		String out= new String();
		for (TableVar i:vars) {
			out += values.get(i.hash) + "\t";
		}		
		return out;
	}
	// return value by key/hash
	public String get(int key) {
		return values.get(key);
	}
}
public class Table {
	// column dictionary
	// key - name hash
	// value - name - index pair
	public ArrayList<TableVar> variables;
	// arraylist stores table values
	public ArrayList<TableSubject> subjects;
	
	public Table() {
		variables = new ArrayList<>();
		subjects = new ArrayList<>();
	}
	// get values of a variable
	public ArrayList<String> getValues(String var) {
		int key = Objects.hash(var);		
		ArrayList<String> out = new ArrayList<>();
		for (TableSubject i:subjects) {
			out.add(i.values.get(key));
		}
		return out;
	}
	public void addVars(String[] row) {
		for(String i:row) {			
			int key = Objects.hash(i);
			TableVar var = new TableVar();
			var.name = i;
			var.hash = key;
			variables.add(var);
		}		
	}
	public void add(String[] row) {
		HashMap<Integer, String> subj = new HashMap<>();
		boolean isNA = false;
		for(int i=0; i<row.length; i++) {
			int key = variables.get(i).hash; 
			subj.put(key, row[i]);
			if(row[i].equals("NA")) 
				isNA = true;			
		}
		if (!isNA)//if any value is NA don't add
			subjects.add(new TableSubject(subj));
	}
	// print table, header and values
	public void print(int from, int to) {
		// from, to - row number
		for(TableVar i:variables) {
			System.out.print(i.toString() + "\t");
		}
		System.out.print("\n");
		for (int i = from - 1 ; i < to; i++ ) {			
			System.out.print(subjects.get(i).toString(variables));			
			System.out.print("\n");
		}
	}
	// print out first 20 rows
	public void print() {
		this.print(1, 20);
	}
	// print selected variables and data
	// print data by indexes array
	// get indexes array as a result of filter function	
	public void printSelected(String... names) {
		ArrayList<Integer> indexes = new ArrayList<>();
		for(int i=0; i<subjects.size(); i++) {
			indexes.add(i);
		}
		int break_point = 10;
		printSelected(indexes, break_point, names);
	}	
	public void printSelected(int break_point, String... names) {
		ArrayList<Integer> indexes = new ArrayList<>();
		for(int i=0; i<subjects.size(); i++) {
			indexes.add(i);
		}
		printSelected(indexes, break_point, names);		
	}
	public void printSelected(ArrayList<Integer> indexes, String... names) {
		int break_point = 10;
		printSelected(indexes, break_point, names);
	}
	public void printSelected(ArrayList<Integer> indexes, int break_point, String... names) {
		// break_point - how many lines of data to print
		// names - column names/variables
		
		// get hash for every name		
		ArrayList<Integer> keys = getVarHash(names);
		
		System.out.print("\t"); // print empty column name
		for (String name:names) {
			for (TableVar i:variables) {					
				// check if names exists in the table				
				if (i.contains(name)) {
					// print column name
					System.out.print(name + "\t");		
				}
			}			
		}		
		System.out.print("\n"); 
		//
		int k = 0; // count printed rows
		// print values for every column using hash
		for (Integer i:indexes) {
			TableSubject subject = subjects.get(i);
			
			System.out.print(i + "\t"); // print row number
			// print value for every key
			for (int key:keys) {			
				System.out.print(subject.get(key) + "\t");
			}
			System.out.print("\n");
			// break loop if shown enough data
			if(k++ >= break_point) {
				System.out.println("... and " + (indexes.size() - break_point) + " more");
				break;
			}
		}		
	}
	
	//get hash key of a variable
	public int getVarHash(String name) {
		ArrayList<Integer> keys = getVarHash(name, null);
		return keys.get(0);
	}
	public ArrayList<Integer> getVarHash(String... names) {
		ArrayList<Integer> keys = new ArrayList<>();
		for (String name:names) {
			for (TableVar i:variables) {
				if (i.contains(name)) {
					keys.add(i.hash);				
				}							
			}			
		}
		return keys;
	}
	
	// filter function	
	public ArrayList<Integer> filter(String variable_name, String filterBy) {
		ArrayList<Integer> indexes = new ArrayList<>();
		for(int i=0; i<subjects.size(); i++) {
			indexes.add(i);
		}
		return this.filter(indexes, variable_name, filterBy);
	}
	// filter with indexes list
	// to filter all subjects use List with all indexes
	public ArrayList<Integer> filter(ArrayList<Integer> indexes, String variable_name, String filterBy){
		// filter column/variable by any string
		// data value must be equal to filterBy param
		// return ArrayList of subjects array indexes
		ArrayList<Integer> result = new ArrayList<>();
		int key = getVarHash(variable_name);
		//
		for (int i=0; i<indexes.size(); i++){
			int index = indexes.get(i);
			TableSubject subject = subjects.get(index);
			if(subject.get(key).equals(filterBy))
				result.add(index);
		}
		return result;		
	}
	public void filterAndPrint(String variable_name, String filterBy, String... names) {
		ArrayList<Integer> indexes = filter(variable_name, filterBy);
		printSelected(indexes, names);
	}
	
	// find minimum of variable values
	// by indexes array
	public double min(ArrayList<Integer> indexes, String variable_name) {
		double result = Integer.MAX_VALUE;
		int key = getVarHash(variable_name);
		// find minimum
		for (Integer i:indexes) {
			TableSubject subject = subjects.get(i);
			int v = Integer.parseInt(subject.get(key)); 
			if (v < result)
				result = v;
		}
		return result;
	}
	// find maximum of variable values
	// by indexes array
	public double max(ArrayList<Integer> indexes, String variable_name) {
		double result = -1;
		int key = getVarHash(variable_name);
		// find minimum
		for (Integer i:indexes) {
			TableSubject subject = subjects.get(i);
			int v = Integer.parseInt(subject.get(key)); 
			if (v > result)
				result = v;
		}
		return result;
	}
	// get the sum of variable values
	// by indexes
	public double sum(ArrayList<Integer> indexes, String variable_name) {
		double result = 0;
		int key = getVarHash(variable_name);
		for (Integer i:indexes) {
			TableSubject subject = subjects.get(i);
			//
			result += Integer.parseInt(subject.get(key));
		}
		return result;
	}
	// get mean of variable values
	// by indexes
	public double mean(ArrayList<Integer> indexes, String variable_name) {
		return this.sum(indexes, variable_name) / indexes.size();
	}	
	
	// get median of variable values
	// by indexes
	public double median(ArrayList<Integer> indexes, String variable_name) {
		double result = -1;
		int key = getVarHash(variable_name);
		// build new array, just values
		ArrayList<Double> values = new ArrayList<>(indexes.size());
		for (Integer i:indexes) {
			TableSubject subject = subjects.get(i);
			//
			double v = Double.valueOf(subject.get(key));
			values.add(v);
		}		
		// sort values array
		Collections.sort(values);
		// find median
		if (values.size() % 2 == 0)
		    result = ((double)values.get(values.size()/2) + (double)values.get(values.size()/2 -1 ))/2;
		else
			result = ((double)values.get(values.size()/2));
		//
		return result;
	}	
	
	// count different occurrences of a value
	// provide variable
	// search within indexes array, to search full dataset provide all indexes
	// return HashTable of value - counter pair
	public HashMap<String, Integer> countEach(ArrayList<Integer> indexes, String variable){
		HashMap<String, Integer> result = new HashMap<>();
		int key = getVarHash(variable);
		//
		for (int i=0; i<indexes.size(); i++){
			int index = indexes.get(i);
			TableSubject subject = subjects.get(index);
			//
			String val = subject.get(key);
			Integer counter = result.get(val);
			if(counter == null)
				result.put(val, 1);// count from 1
			else {
				result.put(val, ++counter);
			}
		}		
		return result;
	}
	public HashMap<String, Integer> countEach(String variable){
		ArrayList<Integer> indexes = new ArrayList<>();
		for(int i=0; i<subjects.size(); i++) {
			indexes.add(i);
		}
		return countEach(indexes, variable);
	}
	
}
