import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Scanner;
import java.util.TreeMap;

// class to represent table variables
// or table header
class FlightVar {
	// variable name
	String name;
	// make hash from var name
	int hash;
	
	public FlightVar() {
	}
	public FlightVar(String _name) {
		name = _name;
		hash = this.hashCode();
	}
	public String toString() {
		return name;
	}
	public boolean contains(String name) {
		if (this.name.equals(name))
			return true;
		else
			return false;		
	}
	@Override
	public int hashCode() {
		return Objects.hash(this.name);
	}
}
// class to represent single flight
class Flight{
	// single row values
	// key is the same hash as for FlightVar to represent data
	HashMap<Integer, String> values;
	// add check if flight is cancelled 
	// get hashcode from this:
	String[] HASH_SOURCE = {"day", "month", "year", "flight"};
	
	boolean cancelled;
	
	public Flight() {
		values = new HashMap<Integer, String>();
		cancelled = false;
	}
	public Flight(HashMap<Integer, String> map){
		values = map;
		cancelled = false;
	}
	public Flight(HashMap<Integer, String> map, boolean _cancelled){
		values = map;
		cancelled = _cancelled;
	}
		
	public String toString() {
		String out= new String();
		for (Integer i:values.keySet()) {
			out += values.get(i) + "\t";
		}
		if (cancelled) 
			out += " cancelled";
		out += "\n";
		return out;
	}
	// return value by key/hash
	public String get(int key) {
		return values.get(key);
	}	
	// check if cancelled
	// return true if not
	// false if cancelled
	public boolean check() {
		if (this.cancelled)
			return false;
		else
			return true;
	}
	@Override
	public int hashCode() {
		// store values it this
		String[] val = new String[HASH_SOURCE.length];
		int j=0;
		for (String i:HASH_SOURCE) {
			// find key to the value
			FlightVar var = new FlightVar(i);
			int key = var.hash;
			// get the value from hash of a single variable as String
			val[j++] = String.valueOf(this.values.get(key));
		}
		return getHashFromValues(val);
	}
	// use quadratic probing then bucket is taken
	public int hashCode(int k) {
		return hashCode() + k*k;
	}
	// helper function to calculate hash value
	public static int getHashFromValues(String[] val) {
		int result = 17; // something to start with
		int STRANGE = 37; // a strange first number
		for(String i:val) {
			result = STRANGE * result + Objects.hash(i); 
		}
		return result;
	}
	
}

class Airport {
	String name;
	int runways;
	
	public Airport(String name, int runways) {
		this.name = name;
		this.runways = runways;
	}
}
public class FlightsScheduler {
	// column dictionary
	// key - name hash
	// value - name - index pair
	public ArrayList<FlightVar> variables;
	// HashMap stores flight data
	public HashMap<Integer, Flight> flights;
	// CSV defaults
	String DEFAULT_SEPARATOR = ",";
	String DEFAULT_QUOTE = "\"";
	// set print break point
	int PRINT_BREAK_POINT = 10;
	//
	public FlightsScheduler() {
		variables = new ArrayList<>();
		flights = new HashMap<>();
	}
	// load csv data into structure
	// n_lines - set how much data to read
	// set n_lines to -1 to read whole file
	public void loadData(String flightDataFile, int n_lines) {

		// get the data
		File f = new File(flightDataFile);

		try (Scanner scanner = new Scanner(f)){	
			int k = 0; // count already red lines
			while(scanner.hasNextLine() && (k < n_lines || n_lines == -1)) {
				String[] tokens = scanner.nextLine().split("\n");
				// read line by line
				for(String i:tokens) {
					// remove quotes
					i = i.replace(DEFAULT_QUOTE, "");
					// split line
					String[] line = i.split(DEFAULT_SEPARATOR);	// get data by split
					// add data
					if (k > 0)
						this.add(line);
					// add table header at first line
					else
						this.addVars(line);
					//					
				}	
				k++;
			}
		} catch (FileNotFoundException ex) {
			System.out.println(ex);
		}				
	}
	public void loadData(String flightDataFile) {	
		this.loadData(flightDataFile, -1);
	}
	private void addVars(String[] row) {
		for(String i:row) {			
			variables.add(new FlightVar(i));
		}		
	}
	// add flights to hashMap
	// add as variables the first line of data
	private void add(String[] row) {
		//HashMap<Integer, String> single_flight = new HashMap<>();
		Flight single_flight = new Flight();
		boolean isNA = false;
		
		for(int i=0; i<row.length; i++) {
			// get hash from variables to keep it the same 
			int key = variables.get(i).hash; 
			// add value
			single_flight.values.put(key, row[i]);
			
			if(row[i].equals("NA")) 
				isNA = true;			
		}
		if (isNA)
			single_flight.cancelled = true;
		
		int k = 0; // to use with open addressing 
		// get flight hash from data
		int flight_hash = single_flight.hashCode(k);				
		
		//check if bucket is taken
		while (this.flights.containsKey(flight_hash)) {		
			k++;
			// get flight hash from data
			flight_hash = single_flight.hashCode(k);				
		}
			
		// add flight to dataset
		this.flights.put(flight_hash, single_flight);
	}
	
	//get hash key of a variable
	public ArrayList<Integer> getVarHash() {
		ArrayList<Integer> all_keys = new ArrayList<>(variables.size());
		for(FlightVar i:variables) {
			all_keys.add(i.hash);
		}
		return all_keys;
	}
	public ArrayList<Integer> getVarHash(String... names) {
		ArrayList<Integer> keys = new ArrayList<>();
		for (String name:names) {
			for (FlightVar i:variables) {
				// check if variable exists
				if (i.contains(name)) {
					keys.add(i.hash);				
				}							
			}			
		}
		return keys;
	}
	// print table, header and values
	public void printAll(int n_lines) {
		// set n_lines to -1 to print all values
		ArrayList<Integer> var_keys = getVarHash();
		// print header
		for(FlightVar i:variables) {
			System.out.print(i.toString() + "\t");
		}
		System.out.print("\n");
		// print values
		int k = 0; // line counter
		for (Flight flight:flights.values()) {
			for (Integer key:var_keys) {
				System.out.print(flight.get(key) + " \t ");				
			}
			if (flight.cancelled)
				System.out.print(" cancelled");
			// end line
			System.out.print("\n");
			if (n_lines != -1 && k++ > n_lines)
				break;
		}
	}
	// print out first 20 rows
	public void print() {
		this.printAll(20);
	}	
	// print selected variables and data
	// print data by flight keys array
	// get keys array as a result of filter function
	private ArrayList<Integer> getAllKeys(){
		// get all keys
		return  new ArrayList<Integer>(this.flights.keySet());
	}
	public void printSelected(String... names) {
		printSelected(this.getAllKeys(), PRINT_BREAK_POINT, names);
	}	
	public void printSelected(ArrayList<Integer> indexes, String... names) {
		printSelected(indexes, PRINT_BREAK_POINT, names);
	}
	// print single flight
	public void printSelected(Integer key, String... names) {
		ArrayList<Integer> indexes = new ArrayList<>(1);
		indexes.add(key);
		printSelected(indexes, PRINT_BREAK_POINT, names);
	}	
	// print table border
	private void printBorder(int column_length, int n_columns) {
		String column_border = "";
		for(int i=0; i<column_length; i++) {
			column_border += "-";
		}
		// print border
		System.out.printf("+");
		for (int i=0; i<n_columns; i++) {
			System.out.printf(column_border);
			System.out.printf("+");
		}
		System.out.printf("%n");
		
	}
	public void printSelected(ArrayList<Integer> keys, int break_point, String... names) {
		// break_point - how many lines of data to print
		// names - column names/variables
		
		// get hash for every name		
		ArrayList<Integer> var_keys = getVarHash(names);
		// print format for table 
		String format = " %-9s|";
		// print border
		printBorder(10, names.length + 1);
		
		System.out.printf("|"+format, "key"); // print hash key column name
		for (String name:names) {
			for (FlightVar i:variables) {					
				// check if names exists in the table				
				if (i.contains(name)) {
					// print column name
					System.out.printf(format, name);
				}
			}			
		}		
		System.out.print("\n");
		// print border
		printBorder(10, names.length + 1);
		//
		int k = 0; // count printed rows
		// print values for every column using hash
		for (Integer i:keys) {
			Flight subject = flights.get(i);
			// break if object not found
			if (subject == null)
				break;
			System.out.printf("|"+format, i); // print row key
			// print value for every key
			for (int var_key:var_keys) {			
				System.out.printf(format, subject.get(var_key));
			}
			System.out.print("\n");
			// break loop if shown enough data
			if(k++ >= break_point) {
				System.out.println("... and " + (keys.size() - break_point) + " more");
				break;
			}			
		}	
		// print border
		printBorder(10, names.length + 1);
		System.out.println();
	}
	// get single flight hash key
	public int find(int day, int month, int year, String flightCode) {
		// cast all to string
		String[] data = {String.valueOf(day), 
				String.valueOf(month), 
				String.valueOf(year), 
				flightCode};
		int hash = Flight.getHashFromValues(data);
		return hash;		
	}

	// Reallocate flight to new airport ECI
	public void reallocate(int day, int month, int year, String flightCode) {		
		int flight_key = find(day, month, year, flightCode);
		this.reallocate(flight_key);
	}
	private void reallocate(int flight_key) {
		String VAR_TO_CHANGE = "origin";
		String NEW_VALUE = "ECI";		
		// get flight from hashmap
		Flight the_flight = this.flights.get(flight_key);
		int var_key = getVarHash(VAR_TO_CHANGE).get(0);
		// change origin to ECI
		the_flight.values.put(var_key, NEW_VALUE);
		// if reallocated, then also cancelled
		the_flight.cancelled = true;		
		// put the flight back into hashmap
		this.flights.put(flight_key, the_flight);		
	}
	// answer true if flight not changed
	// false if cancelled or reallocated
	public boolean check(int day, int month, int year, String flightCode) {
		int flight_key = find(day, month, year, flightCode);
		Flight the_flight = this.flights.get(flight_key);
		if (the_flight.cancelled)
			return false;
		else return true; 
	}	
	// schedule flights with given airports
	// insert airport-runways pairs
	public void rescheduleFlights(Airport[] airports) {
		// add flights to treeMap k->dep_time hash, v-> index 
		// flights will be sorted by departure time
		TreeMap<String, Integer> flights_dep_time = new TreeMap<>();
		// sort flights data by sched_dep_time
		// include year, month and day to sort 
		String[] values_to_hash = { "year", "month", "day", "sched_dep_time", "flight"};
		ArrayList<Integer> values_keys = this.getVarHash(values_to_hash);
		
		for(int  i:this.flights.keySet()) {
			//if (!this.flights.get(i).cancelled) {
				// construct key for treeMap
				//long  hash_key = 0;
				String key_string = new String();
				// construct hash key			
				for (int k:values_keys) {
					// construct key as string
					String val = this.flights.get(i).get(k);
					// reserve 4 digits for each value
					while(val.length() < 4) {
						val = "0" + val;
					}
					key_string += val;
				}
	
				int v = 0;
				String suff = "00"; // add 00 at the end ?
				// check if key is already used			
				while (flights_dep_time.get(key_string + suff) != null) {				
					// increment v if key is used				
			        try {
			        	v = Integer.parseInt(key_string.substring(2));// last two digits
			        } catch (NumberFormatException exception) {
			            // Output expected NumberFormatException.
			            v = 0;
			        }								
					v++;
					suff = String.format("%02d", v);
				}
				key_string += suff; // add suffix at the end	
				
				// add key to the tree
				flights_dep_time.put(key_string, i);			
			//}
		}
		
		// keep ECI flights keys in this:
		ArrayList<Integer> ECI_flights = new ArrayList<>();

		
		int[] counter = new int[airports.length];// to count each airport runway in a loop
		int full = 0; // count full runways
		int n = 0; // loop counter 
		// update origin for flights
		// to add ECI airport
		
		for(Map.Entry<String, Integer> flight:flights_dep_time.entrySet()) {
			int flight_index = flight.getValue();

			int a = n % airports.length; //airport array index
			// fill all runways 			
			while (counter[a] > 0 && (counter[a] % airports[a].runways == 0)) {
				// when all runways are full - start again
				if (full++ == airports.length) {
					for(int i=0; i < airports.length; i++) {
						counter[i] = 0;
					}
					full = 0;
				}					
				n++;
				a = n % airports.length; 
			} 
			counter[a]++;
			String airport = airports[a].name;

			// update origin of a flight
			Flight single_flight = this.flights.get(flight_index);
			int val_key = this.getVarHash("origin").get(0);
			single_flight.values.put(val_key, airport);
			
			// record ECI flights
			if (airports[a].name == "ECI") {
				ECI_flights.add(flight_index);
				// mark flight cancelled
				single_flight.cancelled = true;
			}	
			
			// put it back
			this.flights.put(flight_index, single_flight);

			// increment loop
			n++;
		}
		System.out.println("====== ECI flights ======");
		System.out.println("Total ECI flights: " + ECI_flights.size());
		System.out.println();				
	}
}
