import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.TreeMap;


public class FlightGraph extends FlightsScheduler{
	Graph graph;
	
	public FlightGraph() {
		super();
		this.graph = new Graph();
	}
	public FlightGraph(String fileName) {
		loadData(fileName, -1);
		this.graph = new Graph();
		this.createGraph();
	}	
	public FlightGraph(String fileName, int n_lines) {
		loadData(fileName, n_lines);
		this.graph = new Graph();
		this.createGraph();
	}	
	private void createGraph() {
		// what variables to use as nodes ?
		int origin_key = getVarHash("origin").get(0);
		int dest_key = getVarHash("dest").get(0);
		
		// for every flight
		for (Flight flight:flights.values()){
			// watch out for cancelled flights !
			if (!flight.cancelled) {
				// get flight values as nodes
				String origin = flight.get(origin_key);
				String dest = flight.get(dest_key);
				// add edge undirected
				this.graph.addEdge(origin, dest);
			}
		}
		
	}
	public void print() {
		this.graph.printGraph();
	}
	
	// count how many destinations
	// when taking "legs" number of flights
	public ArrayList<String> countDest(String origin, int legs) {
		// level 0 - origin		
		// level 1 - direct edges
		// level 2 - edges of 2nd degree
		int level = 0;
		
		// declare Queue for searching nodes
		// each level is in a separate Set
		// use hash set to store unique node names
		ArrayList < HashSet<String> > Q = new ArrayList < HashSet<String> >();
		for (int i=0; i<=legs; i++) {
			Q.add(new HashSet<String>());
		}
				
		// level 0 node
		Q.get(0).add(origin);
		//level 0 - legs
		while (level < legs) {
			//System.out.println("== level: " + (level+1));
			
			for (String node:Q.get(level)) {	
				//System.out.println("from " + node + " to: ");
				Edge edgesList = this.graph.nodes.get(node);
				
				while(edgesList != null) {
					String next_node = edgesList.node_name;
					// don't go back to origin
					if(!next_node.equals(origin))
						Q.get(level + 1).add(next_node);
					//print
					//System.out.print(next_node + ", ");
					edgesList = edgesList.nextNeighbour;
				}
				//System.out.print("\n");
			}
			level++;
		}
		for(int i=0; i<Q.size(); i++) {
			System.out.println("Q level " + i + " size: " + Q.get(i).size());
		}
		// return arraylist of airport names
		return new ArrayList<String>(Q.get(legs));
	}
	// filter function	
	public ArrayList<Integer> filter(String variable_name, String filterBy) {
		ArrayList<Integer> indexes = new ArrayList<>();
		for(int f:this.flights.keySet()) {
			indexes.add(f);
		}
		return this.filter(indexes, variable_name, filterBy);
	}
	// filter with indexes list
	// to filter all subjects use List with all indexes
	public ArrayList<Integer> filter(ArrayList<Integer> indexes, String variable_name, String filterBy){
		// filter column/variable by any string
		// data value must be equal to filterBy param
		// return ArrayList of subjects array indexes
		ArrayList<Integer> result = new ArrayList<>();
		int key = getVarHash(variable_name).get(0);
		//
		for (int i=0; i<indexes.size(); i++){
			int index = indexes.get(i);
			Flight subject = flights.get(index);
			if(subject.get(key).equals(filterBy))
				result.add(index);
		}
		return result;		
	}	

}
