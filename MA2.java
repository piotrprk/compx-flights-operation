import java.util.ArrayList;
import java.util.Calendar;
import java.util.Map;

public class MA2 {

	public static void main(String[] args) {
		// get file
		String file_name = "flights-dataset.csv";
		/*
		// get the data
		CSVFileIO fileO = new CSVFileIO(file_name);
		
		ArrayList<String[]> input_data = fileO.readCSV();	
		
		// initialize Table
		Table flights = new Table();	
		
		for(int i=0; i < input_data.size(); i++) {
			String[] line = input_data.get(i);
			//
			if (i > 0)
				flights.add(line);
			// add table header at first line
			else
				flights.addVars(line);
			//
		}
		// how many imported ?
		System.out.println("imported: " + flights.subjects.size() + " subjects");
		*/
/*		
		// print table, 20 rows
		//flights.print();
		System.out.println();
		System.out.println("Variables: " + flights.variables.size());
		System.out.println("Subjects: " + flights.subjects.size());
		System.out.println();
		//flights.printSelected(20, "carrier", "origin", "distance");
		// find flight origin EWR 
		//flights.filterAndPrint("origin", "EWR", "origin", "carrier");
		String[] origin_search = {"EWR", "JFK", "LGA"};
		for (String org:origin_search) {
			ArrayList<Integer> result = flights.filter("origin", org);
			flights.printSelected(result, "tailnum", "origin", "dest", "distance");
			System.out.println("flights from " +org + " : " + result.size());
			// minimum distance
			String v = "distance";
			System.out.println("minimum " + v + " : " + flights.min(result, v));
			// max distance
			System.out.println("maximum " + v + " : " + flights.max(result, v));			
			// mean
			System.out.println("mean  " + v + " : " + flights.mean(result, v));			
			// median
			System.out.println("median " + v + " : " + flights.median(result, v));
		}
		// PART 2
		System.out.println();
		System.out.println("PART 2"); 
		System.out.println();
		String[] airline_search = {"UA", "HA", "B6"};
		String v = "tailnum";
		
		for (String airline:airline_search) {
			System.out.println("====" + airline +"====" ); 
			ArrayList<Integer> result = flights.filter("carrier", airline);
			flights.printSelected(result, "tailnum", "origin", "dest", "month", "carrier");
			//
	
			System.out.println("aircrafts "+v+" of " +airline + " : " + result.size());
			// count unique tailnum
			HashMap<String, Integer> unique = flights.countEach(result, "tailnum");
			System.out.println();
			System.out.println("unique " + v + " of " + airline + " : " + unique);
			System.out.println("unique aircrafts "+v+" of " +airline + " : " + unique.size());
			
			//
			// check for destinations in December by airline
			// count each destination
			System.out.println("------------------ unique destinations in Dec ---------------------");
			
			ArrayList<Integer> december = flights.filter(result, "month", "12");
			flights.printSelected(december, "tailnum", "origin", "dest", "month", "carrier");
			// count each destination
			HashMap<String, Integer> unique_dest = flights.countEach(december, "dest");
			System.out.println("unique destinations in Dec.: " + unique_dest);
			System.out.println("unique destinations in Dec for " + airline + " count: " + unique_dest.size());
		}
		
		// question 5
		System.out.println("======= Question 5 =======");
		// count unique destinations
		HashMap<String, Integer> unique_dest = flights.countEach("dest");
		// filter by origin LGA
		ArrayList<Integer> origin_LGA = flights.filter("origin", "LGA");
		// count unique destinations for LGA
		HashMap<String, Integer> unique_dest_lga = flights.countEach(origin_LGA, "dest");
		// result 
		System.out.println(
				"How many of the airports listed in the dataset cannot be reached directly from LGA by taking a single flight? ");
		System.out.println("Answer: " + (unique_dest.size() - unique_dest_lga.size()));
*/
		
		/*
		// PART 3
		System.out.println();
		System.out.println("PART 3"); 
		System.out.println();		
		
		// define new airport and runways number
		ArrayList<Pair> runways = new ArrayList<>();
		runways.add(new Pair("EWR", 3));
		runways.add(new Pair("LGA", 2));
		runways.add(new Pair ("JFK", 4));
		runways.add(new Pair("ECI", 4));
		
		// add flights to treeMap k->dep_time hash, v-> index 
		// flights will be sorted by departure time
		TreeMap<String, Integer> flights_dep_time = new TreeMap<>();
		// sort flights data by sched_dep_time
		// include year, month and day to sort 
		// and flight number
		String[] values_to_hash = { "year", "month", "day", "sched_dep_time", "flight"};
		ArrayList<Integer> values_keys = flights.getVarHash(values_to_hash);
		int ind = 0; //index counter		
		for(TableSubject  i:flights.subjects) {
			// construct key for treeMap
			String key_string = new String();
			// construct hash key			
			for (int k:values_keys) {
				// construct key as string
				String val = i.get(k);
				// reserve 4 digits for each value
				while(val.length() < 4) {
					val = "0" + val;
				}
				key_string += val;
			}

			int n = 0; // counter for duplicate hash keys
			String suff = String.format("%02d", n); // add 00 at the end ?
			// check if key is already used			
			while (flights_dep_time.get(key_string + suff) != null) {				
				// increment if key is used				
		        try {
		        	n = Integer.parseInt(key_string.substring(2));// last two digits
		        } catch (NumberFormatException exception) {
		            // Output expected NumberFormatException.
		            n = 0;
		        }								
				n++;
				suff = String.format("%02d", n);
			}
			key_string += suff;			
			
			// add key to the tree
			flights_dep_time.put(key_string, ind++);
		}
		//check if nothing is missing
		System.out.println("Tree size: " + flights_dep_time.size()); 
		// print flights to check sorting
		ArrayList<Integer> list = new ArrayList<>(flights_dep_time.values());		
		flights.printSelected(list, "month", "day", "sched_dep_time", "origin", "dest", "tailnum", "flight");
		
		// declare map to store index, origin pairs
		HashMap <Integer, String> index_origin_map = new HashMap<>(flights.subjects.size());
		// keep ECI flights indexes in this:
		ArrayList<Integer> ECI_flights = new ArrayList<>();

		
		int[] counter = new int[runways.size()];// to count each airport runway in a loop
		int full = 0; // count full runways
		int n = 0; // loop counter 
		// update origin for flights
		// to add ECI airport
		
		for(Map.Entry<String, Integer> flight:flights_dep_time.entrySet()) {
			int flight_index = flight.getValue();

			int a = n % runways.size(); //airport array index
			// fill all runways 			
			while (counter[a]>0 && (counter[a] % runways.get(a).value == 0)) {
				// when all runways are full - start again
				if (full++ == runways.size()) {
					for(int i=0; i<runways.size(); i++) {
						counter[i] = 0;
					}
					full = 0;
				}					
				n++;
				a = n % runways.size(); 
			} 
			counter[a]++;
			String airport = runways.get(a).name;
			// record ECI flights
			if (a == 3)
				ECI_flights.add(flight_index);
			// save flight-origin pair
			index_origin_map.put(flight_index, airport);
			
			n++;
		}
		
		// display result
		int break_point = 10; // break loop after
		int c = 0;
		System.out.println("index \t org \t old \t dest \t tailnum");
		for(Entry<String, Integer> flight:flights_dep_time.entrySet()) {
			// index
			int index = flight.getValue();
			// new origin
			String org = index_origin_map.get(index);
			// old_origin
			String ol_org = flights.subjects.get(index).get(flights.getVarHash("origin"));
			// destination
			String dest = flights.subjects.get(index).get(flights.getVarHash("dest"));
			// tailnum
			String tailnum = flights.subjects.get(index).get(flights.getVarHash("tailnum"));
			//
			System.out.print( index + " \t " + org + " \t " + ol_org + " \t " + dest + " \t " + tailnum + " \n" );
			// break loop
			if(c++ > break_point)
				break;
		}
		
		System.out.println("====== ECI flights ======");
		System.out.println("Total ECI flights: " + ECI_flights.size());
		//
		// alternative - proportional
		//
		System.out.println("=== Alternative method ===");
		String[] origin_search = {"EWR", "JFK", "LGA"};
		ArrayList<Integer> origin_count = new ArrayList<>(origin_search.length);
		int total = 0;
		for (String org:origin_search) {
			ArrayList<Integer> result = flights.filter("origin", org);
			origin_count.add( result.size() );
			total += result.size();
			System.out.println("flights from " + org + " : " + result.size());
		}
		System.out.println("Total flights: " + total); 
		// take into account the runways number
		// look at old runways number
		ArrayList<Pair> old_runways = new ArrayList<>(runways.subList(0, runways.size()-1));
		int old_runways_total = 0;
		String old_runways_list = new String();
		for (Pair i:old_runways) {
			old_runways_total += i.value;
			old_runways_list += i.name + ", ";
		}
		System.out.println("Runways before: " + old_runways_list + " total number: " + old_runways_total);
		int new_runways_total = 0;
		String new_runways_list = new String();
		for (Pair i:runways) {
			new_runways_total += i.value;
			new_runways_list += i.name + ", ";
		}
		System.out.println("Runways new deal: " + new_runways_list + " total number: " + new_runways_total);
		// calculate proportions
		int sum_proportions = 0;
		for(int i=0; i<origin_count.size(); i++) {
			int prop = Math.round((float)origin_count.get(i) * old_runways_total / new_runways_total);
			sum_proportions += prop;
			System.out.println("new flights number from " + runways.get(i).name + " : " + prop);
		}
		System.out.println("sum flight from 'old' airports: " + sum_proportions);
		// for new airport ECI number o flights = total - sum_proportions
		int answer_q1 = total - sum_proportions;
		System.out.println("Answer: Total ECI flights: " + answer_q1);
		*/
		/*

		//PART 3 - 4
		System.out.println("");
		System.out.println("====== Flight Scheduler ======");
		FlightsScheduler flightScheduler = new FlightsScheduler();
		// load CSV data
		flightScheduler.loadData(file_name, 1000);
		
		flightScheduler.printSelected("day", "month", "flight", "tailnum", "origin", "dest");
		// find single
		System.out.println("====== Single flight reallocate ======");
		String flightCode = "1757";
		int single = flightScheduler.find(1, 1, 2013, flightCode);
		flightScheduler.printSelected(single, "day", "month", "flight", "tailnum", "origin", "dest");
		// reallocate single flight
		flightScheduler.reallocate(1, 1, 2013, flightCode );
		flightScheduler.printSelected(single, "day", "month", "flight", "tailnum", "origin", "dest");		
		// check flight
		System.out.println("====== Single flight check ======");
		boolean check = flightScheduler.check(1, 1, 2013, flightCode );
		System.out.println("flight " + flightCode + " " + (check ? "OK" : "cancelled"));
		//
		// reschedule flights
		// define airports and runways number
		Airport[] airports = new Airport[4];
		airports[0] = new Airport("EWR", 3);
		airports[1] = new Airport("LGA", 2);
		airports[2] = new Airport("JFK", 4);
		airports[3] = new Airport("ECI", 4);		
		//
		System.out.println();
		// print before
		flightScheduler.printSelected("year", "month", "day", "flight", "tailnum", "origin", "dest");		
		//
		flightScheduler.rescheduleFlights(airports);
		//
		flightScheduler.printSelected("year", "month", "day", "flight", "tailnum", "origin", "dest");		
		*/
		/*
		// PART 4 Graph
		System.out.println();
		System.out.println("PART 4"); 	
		System.out.println("====== Graph ======");
		System.out.println();			
		// load data
		FlightGraph flightGraph = new FlightGraph(file_name, 1000);		
		// print graph
		//flightGraph.print();
		
		//flightGraph.graph.printNode("JFK");
		//flightGraph.graph.printNode("EWR");
		
		// count all edges
		int count = 0;
		for (String key:flightGraph.graph.nodes.keySet()) {
			Edge e = flightGraph.graph.nodes.get(key);
			count += e.weight;		
			while(e.nextNeighbour != null) {
				e = e.nextNeighbour;
				count += e.weight;
			}
		}
		System.out.println("all edges: " + count);
		// Q 2 - 4
		System.out.println();
		ArrayList<String> answer = new ArrayList<>();
		answer = flightGraph.countDest("EWR", 2);
		System.out.println("Count dest. from EWR, legs 2 ");
		System.out.println("dest found: ");
		for (String a:answer) {
			System.out.print(a + ", ");
		}
		System.out.print("\n");
		System.out.println("Answer - count: " + answer.size());
		//
		System.out.println();
		answer = flightGraph.countDest("EWR", 3);
		System.out.println("Count dest. from EWR, legs 3");
		System.out.println("dest found: ");
		for (String a:answer) {
			System.out.print(a + ", ");
		}		
		System.out.print("\n");		
		System.out.println("Answer - count: " + answer.size());
		*/
		// PART 5
		System.out.println();
		System.out.println("PART 5"); 	
		System.out.println();
		//
		NameTimecodeGraph graph = new NameTimecodeGraph();
		// load data
		FlightsScheduler flights = new FlightsScheduler();
		flights.loadData(file_name);			
		// create graph
		System.out.println("loaded from file: " + flights.flights.size());
		// get flight date/time values
		String[] values_to_hash = { "year", "month", "day", "dep_time", "arr_time", "origin", "dest"};
		ArrayList<Integer> value_keys = flights.getVarHash(values_to_hash);
		// count imported flights
		int count_imported = 0;

		// create graph
		for (Map.Entry<Integer, Flight> flight_set:flights.flights.entrySet()){
			Flight flight = flight_set.getValue();
			//int flight_index = flight_set.getKey();
			// watch out for cancelled flights !
			if (!flight.cancelled) {
				count_imported ++;
				// get timecode of arrival for every flight
				// calculate flight time
				int year = Integer.valueOf(flight.get(value_keys.get(0)));
				int month = Integer.valueOf(flight.get(value_keys.get(1))) - 1;// java.util.Calendar.MONTH starts from 0! 
				int day = Integer.valueOf(flight.get(value_keys.get(2)));
				int dep_hour = Integer.valueOf(flight.get(value_keys.get(3))) / 100;
				int dep_minute = Integer.valueOf(flight.get(value_keys.get(3))) % 100;
				int arr_hour = Integer.valueOf(flight.get(value_keys.get(4))) / 100;
				int arr_minute = Integer.valueOf(flight.get(value_keys.get(4))) % 100;
				
				Calendar dep_date_time = Calendar.getInstance();
				dep_date_time.set(year, month, day, dep_hour, dep_minute);
							
				Calendar arr_date_time = Calendar.getInstance();
				arr_date_time.set(year, month, day, arr_hour, arr_minute);
				// what if arrival is after midnight
				if (arr_hour < dep_hour) {// look for arrival after midnight and add a day
					arr_date_time.add(Calendar.DATE, 1); // add a day				
				}

				// flight time in milliseconds from the Epoch
				long diff = (arr_date_time.getTimeInMillis() - dep_date_time.getTimeInMillis());

				// reverse flight arrival date-time
				// inboud flight arrival + flight time
				Calendar rev_flight_date_time = Calendar.getInstance();
				rev_flight_date_time.setTimeInMillis(arr_date_time.getTimeInMillis() + diff);
				
				// add node and edges to the graph
				String origin = flight.get(value_keys.get(5));
				String destination = flight.get(value_keys.get(6));
				// out flight edge
				graph.addEdge(origin, destination, dep_date_time, arr_date_time);
				// reverse edge
				graph.addEdge(destination, origin, arr_date_time, rev_flight_date_time);
			}
		}
		// print graph
		//graph.printGraph();
		//  count edges to check imported
		int count = 0;
		for (GraphNode n:graph.nodes.values()) {
			for(GraphEdge e:n.edges) {
				count ++;
			}
		}
		System.out.println("all edges count : " + count);
		System.out.println("all imported flights count : " + count_imported);
		System.out.println("check: " + ( (count/2 != count_imported) ? " import ERROR " : " ALL imported "));
		//
		/*
		int i=0; 
		for (GraphEdge e:single_node.edges) {
			System.out.println("edge: " + (i++) + " " + e.toString());
			
		}
		*/
		// MST - calculate max possible flights in 2013
		// set starting point
		String[] start_at = {"EWR", "LGA", "JFK"};		
		// Initialise travel list for every start airport
		ArrayList< ArrayList<GraphEdge> > S = new ArrayList<>();
		for (int i=0; i< start_at.length; i++) {
			S.add( new ArrayList<>() );
		}
		// count to check
		// print edges count
		int all_edges = 0;
		for (int i=0; i< start_at.length; i++) {
			String single = start_at[i];
			GraphNode single_node = graph.nodes.get(single);
			System.out.println("Node: " + single);
			System.out.println("count edges: " + single_node.edges.size());
			all_edges += single_node.edges.size();
		}
		System.out.println("count ALL edges: " + all_edges);

		// 1. add starting point to S
		//S.add(new GraphEdge(start_at, start, start));
		
		// calculate it for every starting point
		for (int i=0; i< start_at.length; i++) {
			// set starting point
			Calendar start = Calendar.getInstance();
			start.set(2013, 0, 1, 5, 0); // 2013-1-1 5:00

			// set end point
			Calendar end = Calendar.getInstance();
			end.set(2013, 11, 31, 23, 59); // 2013-12-31 23:59
			
			// track point in time
			Calendar current = start;
			// current airport
			String current_airport = start_at[i];			

			//  travel until reached the end			
			while (current.getTimeInMillis() < end.getTimeInMillis()) {
				GraphNode current_node = graph.nodes.get(current_airport);
				// find lowest cost (time span) of travel
				long min_cost = Long.MAX_VALUE; // start at infinity
				//
				boolean found = false;

				GraphEdge min_cost_edge = new GraphEdge(current_airport, start, current);
				long current_time = current.getTimeInMillis();
				
				// traverse edges
				for(GraphEdge e:current_node.edges) {
					long departure_time = e.departure.getTimeInMillis();
					long arrival_time = e.arrival.getTimeInMillis();
					// can't travel back in time !
					if ( departure_time >= current_time) {
						// travel time
						long edge_travel = arrival_time - current_time;
						
						if( edge_travel < min_cost) {
							min_cost = edge_travel;
							// travel to min-cost node
							min_cost_edge = e;
							found = true;
						}
					}
				}
				if (found) {
					current_airport = min_cost_edge.node_name;
					current = min_cost_edge.arrival;	
					// add this travel leg to S
					S.get(i).add(min_cost_edge);
				} else {
					break; // break if stuck
				}
			}
		}
		// display S
		System.out.println("=== TRAVEL : ");
		int max_num_flights = 0;
		for (int i=0; i< start_at.length; i++) {			
			System.out.print("\n");
			System.out.println("All flights from " + start_at[i] + ": " + 
					S.get(i).size() + 
					"\n first leg: " + S.get(i).get(0) +
					"\n end at: " + S.get(i).get(S.get(i).size()-1)
					);
			if (S.get(i).size() > max_num_flights)
				max_num_flights = S.get(i).size();

		}
		System.out.println("Maximum number of flights you could have taken in 2013: " + max_num_flights);
		
	}
}
