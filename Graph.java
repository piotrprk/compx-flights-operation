import java.util.HashMap;


class Edge{
	String node_name;
	public int weight;
	Edge nextNeighbour;
	public Edge(String node, Edge nextEdge) {
		this.node_name = node;
		this.weight = 1;
		this.nextNeighbour = nextEdge;
	}
	public Edge(String node, int weight, Edge nextEdge) {
		this.node_name = node;
		this.weight = weight;
		this.nextNeighbour = nextEdge;
	}	
	public void setWeight(int weight) {
		this.weight = weight;
	}
	public String toString() {
		return "-"+weight+"- "+node_name + ((this.nextNeighbour != null) ? ", " + this.nextNeighbour : "");
	}
}

public class Graph {
	// store nodes in a hashmap
	// key == Node name
	HashMap<String, Edge> nodes;
	
	public Graph() {
		nodes = new HashMap<>();		
	}
	public String toString() {
		String result = "";
		for (String e:nodes.keySet()) {
			result += e + " => " + nodes.get(e);
			result += ", ";
		}
		return result;
	}
	public void addEdgeDir(String a, String b) {
		// add node if don't exists
		if(! this.nodes.containsKey(a)) {
			this.nodes.put(a, null);
		}
		if(! this.nodes.containsKey(b)) {
			this.nodes.put(b, null);
		}	
		// edge exists ?
		Edge edge = this.nodes.get(a);
		boolean found = false;
		// look for it
		while (edge != null) {
			if (edge.node_name.equals( b )) {
				found = true;
				edge.weight++;
				break;
			} else {
				edge = edge.nextNeighbour;
			}
		}
		if (!found) {
			edge = new Edge(b, this.nodes.get(a));
			// set edge into hash map
			this.nodes.put(a, edge);	
		}
	}
	public void addEdgeUdir(String a, String b) {
		this.addEdgeDir(a, b);
		this.addEdgeDir(b, a);
	}
	public void addEdge(String a, String b) {
		this.addEdgeUdir(a, b);
	}
	public void printGraph() {
		for (String node:this.nodes.keySet()) {
			this.printNode(node);
		}
	}
	public void printNode(String node) {
		System.out.println(node + " => " + this.nodes.get(node).toString());		
	}	
}
