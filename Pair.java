//define pair class for String-int pairs
public class Pair {
	String name;
	int value;
	public Pair(String s, int v) {
		name = s;
		value = v;
	}
}
