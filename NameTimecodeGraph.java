import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Objects;

class GraphEdge{
	String node_name;
	// timestamp of departure
	Calendar departure;
	// timestamp of arrival
	Calendar arrival;
	// flight index
	int index;

	public GraphEdge(String node) {
		this.node_name = node;
		this.arrival = Calendar.getInstance();	
	}
	public GraphEdge(String node, Calendar departure, Calendar arrival) {
		this.node_name = node;
		this.departure = departure;	
		this.arrival = arrival;	
	}
	public GraphEdge(String node, long departure, long arrival) {
		this.node_name = node;
		this.departure = Calendar.getInstance();
		this.departure.setTimeInMillis(departure);		
		this.arrival = Calendar.getInstance();
		this.arrival.setTimeInMillis(arrival);
		this.index = -1;
	}
	
	public String toString() {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		return node_name + " " +
				"[" + format.format(departure.getTimeInMillis()) + "]" +
				" -> " +
				"[" + format.format(arrival.getTimeInMillis()) + "]";
	}
}
class GraphNode {
	String name;
	// keep edges in a linked list
	LinkedList<GraphEdge> edges;

	public GraphNode(String name) {
		this.name = name;
		this.edges = new LinkedList<GraphEdge>(); 
	}
	
	public String toString() {
		String out = this.name + " : ";
		for (GraphEdge e:this.edges) {
			out += e + ", ";
		}
		return out;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(name, edges);
	}
}

public class NameTimecodeGraph {
	// store nodes in a hashMap of nodes
	// key == Node hash
	HashMap<String, GraphNode> nodes;
	
	public NameTimecodeGraph() {
		nodes = new HashMap<>();		
	}
	public String toString() {
		String result = "";
		for (String e:nodes.keySet()) {
			result += e + " => " + nodes.get(e) + ", ";
		}
		return result;
	}
	// add edge and nodes if they don't exists
	public void addEdge(String a, String b, Calendar departure, Calendar arrival) {	
		// add node if don't exists
		if(! this.nodes.containsKey(a)) {
			GraphNode a_node = new GraphNode(a);
			this.nodes.put(a, a_node);
		}
		if(! this.nodes.containsKey(b)) {
			GraphNode b_node = new GraphNode(b);
			this.nodes.put(b, b_node);
		}	
		// declare new edge
		GraphEdge new_edge = new GraphEdge(b, departure, arrival);
		
		// add edge
		this.nodes.get(a).edges.add(new_edge);
	}

	public void printGraph() {
		for (GraphNode node:this.nodes.values()) {
			this.printNode(node);
		}
	}
	public void printNode(GraphNode node) {
		System.out.println(node);		
	}	
	
}
